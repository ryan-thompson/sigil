#!/usr/bin/node --harmony
"use strict";

let restify     = require('restify');
let fs          = require('fs');
let jws         = require('jws');
let path        = require('path');
let ad          = require('./ad');

let public_key = fs.readFileSync(`${__dirname}/pem.pub`).toString('utf8');
let private_key = fs.readFileSync(`${__dirname}/key`).toString('utf8');

const SIG = 'RS256';
const PORT = 4321;

var server = restify.createServer({
    name: 'sigil',
    version: '1.0.0'
});

server.use(restify.bodyParser({ mapParams: false }));

server.post('/', function(req, res, next) {

    var payload = req.body.payload;
    var login = req.body.login;

    ad.authenticate_user(login, function(err, user){

        if(err){
            res.send({err: err});
        }
        else{
            var signature = jws.sign({
                header: {
                    alg: SIG,
                    signed_on: Date(),
                    signed_by: user.displayName,
                    email: user.mail,
                    from_address: req.connection.remoteAddress
                },
                payload: payload,
                privateKey: private_key
            });

            res.send({signature: signature});
        }
    });

    return next();
});


server.post('/verify', function(req, res, next) {
    let signature = req.body;
    let valid = jws.verify(signature.signature, SIG, public_key);
    res.send({valid: valid});
});


server.post('/view', function(req, res, next){
    let signature = req.body;
    let decoded = jws.decode(signature.signature, SIG);
    res.send({
        header: decoded.header,
        payload: JSON.parse(decoded.payload)
    });
});


server.listen(PORT, function() {
    console.log(`listening on port ${PORT}`);
})



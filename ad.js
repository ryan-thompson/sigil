'use strict';

let active_directory = require('activedirectory');

function authenticate_user(login, callback){
    let config = {
        url: 'ldap://edm-goa-dc-9.goa.ds.gov.ab.ca:389',
        baseDN: 'DC=goa,DC=ds,DC=gov,DC=ab,DC=ca',
        username: `GOA\\${login.user_name}`,
        password: login.password
    };
    let ad = new active_directory(config);

    ad.findUser(login.user_name, function(err,ret){
        callback(err,ret);            
    });
}

module.exports = {
    authenticate_user: authenticate_user
}
